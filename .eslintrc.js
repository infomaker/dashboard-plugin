module.exports = {
    "parser": "babel-eslint",
    "env": {
        "browser": true,
        "commonjs": true,
        "es6": true
    },
    "extends": [
        "eslint:recommended",
        "react-app"
    ],
    "parserOptions": {
        "ecmaFeatures": {
            "experimentalObjectRestSpread": true,
            "jsx": true
        },
        "sourceType": "module"
    },
    "plugins": [
        "react",
        "react-hooks"
    ],
    "ignorePatterns": ["**/*.d.ts"],
    "rules": {
        "indent": [2, 4, { "SwitchCase": 1 }],
        "react/jsx-uses-react": [2],
        "react/jsx-uses-vars": [2],
        "no-case-declarations": 0,
        "no-console": 0,
        "no-unused-vars": 1,
        "no-template-curly-in-string": ["off"],
        "no-use-before-define": "off",
        "@typescript-eslint/no-use-before-define": ["off"]
    }
}