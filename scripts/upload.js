/* eslint-env node */

const fs = require('fs')
const zlib = require('zlib')
const AWS = require('aws-sdk')
const colors = require('colors/safe')

const {
    resolve,
    extname
} = require('path')

const {
    readFile,
    readFileSync
} = fs

const {
    readdir
} = fs.promises

const constants = require('./constants')

let args = {}

process.argv.forEach(arg => {
    const parts = arg.split('=')

    if (parts[0] && parts[1] && parts.indexOf(parts[0]) > -1) {
        args[parts[0]] = parts[1]
    }
})

const s3BucketName = args.S3_BUCKET_NAME
const s3BucketRegion = args.S3_REGION
const s3AccessKey = args.S3_ACCESS_KEY
const s3SecretAccessKey = args.S3_SECRET_ACCESS_KEY

const versionOverride = args.VERSION_OVERRIDE
const localesSourceVersion = args.LOCALES_SOURCE_VERSION
const globalCloudFrontPath = args.GLOBAL_CLOUDFRONT_PATH

if (
    !s3BucketName ||
    !s3BucketRegion ||
    !s3AccessKey ||
    !s3SecretAccessKey
) {
    console.log('\n')
    console.log(colors.red('*******************'))
    console.log(colors.red('       ERROR       '))
    console.log(colors.red('*******************'))
    console.log(colors.red('Missing required variables!'))
    console.log(colors.red('Make sure that all the following varaibles are passed:\n'))
    console.log(colors.red('\t- S3_BUCKET_NAME\n'))
    console.log(colors.red('\t- S3_REGION\n'))
    console.log(colors.red('\t- S3_ACCESS_KEY\n'))
    console.log(colors.red('\t- S3_SECRET_ACCESS_KEY\n'))
} else {    
    const s3Bucket = new AWS.S3({
        region: s3BucketRegion,
        accessKeyId: s3AccessKey,
        secretAccessKey: s3SecretAccessKey
    })

    let locations = {}
    let manifest = require(resolve(constants.MANIFEST_PATH))
    
    const parsePath = value => value.replace(/\./g, '-').toLowerCase()
    
    const bundlePath = parsePath(manifest.bundle)
    const versionPath = parsePath(manifest.version)
    
    const baseKey = `${bundlePath}/${versionOverride ? parsePath(versionOverride) : versionPath}`
    
    const buildGlobalPath = (key, fileLocation) => {
        if (globalCloudFrontPath) {
            return `${globalCloudFrontPath}/${baseKey}/${key}`
        } else {
            return fileLocation
        }
    }
    
    const getPluginFilenames = async (dirPath, prefex = '') => {
        const files = await readdir(dirPath, { withFileTypes: true })
    
        const results =  await Promise.all(files.map((file) => {
            return file.isDirectory() ? getPluginFilenames(`${dirPath}/${file.name}`, `${prefex}${file.name}/`) : `${prefex}${file.name}`
        }))
    
        return results.flat()
    }
    
    const getContentType = ext => {
        switch(ext) {
            case '.js':
                return 'text/javascript'
                
            case '.css':
                return 'text/css'
    
            case '.json':
                return 'application/json'
    
            case '.txt':
                return 'text/plain'
    
            case '.md':
                return 'text/markdown'
    
            case '.png':
                return 'image/png'
    
            default:
                throw new Error('Unkown file ext: ', ext)
        }
    }
    
    const getFile = filePath => {
        return new Promise((_resolve, _reject) => {
            console.log(`${colors.yellow(`Processing file: ${filePath}`)}`)
    
            const ext = extname(filePath).toLocaleLowerCase()
            const contentType = getContentType(ext)
    
            const fileResolver = encoding => {
                return (error, result) => {
                    if (error) {
                        _reject(error)
                    } else {
                        _resolve({
                            file: result,
                            contentType: contentType,
                            contentEncoding: encoding
                        })
                    }
                }
            }
    
            switch(ext) {
                case '.js':                
                case '.css':
                    zlib.gzip(readFileSync(resolve(constants.DIST_PATH, filePath)), fileResolver('gzip'))
                    break
                
                default:
                    readFile(resolve(constants.DIST_PATH, filePath), fileResolver())
            }
        })
    }
    
    const processFileAndUpload = filename => {
        return async (_resolve, _reject) => {
            try {
                const result = await getFile(filename)
    
                const fileLocation = await upload({
                    key: filename,
                    data: result.file,
                    contentType: result.contentType,
                    contentEncoding: result.contentEncoding
                })
    
                locations[filename] = buildGlobalPath(filename, fileLocation)
    
                _resolve()
            } catch(error) {
                _reject(error)
            }
        }
    }
    
    const procesLocaleAndUpload = locale => {
        return async (_resolve, _reject) => {
            try {
                const fileLocation = await upload({
                    key: `locales/${locale.name}`,
                    data: typeof locale.data !== 'string' ? JSON.stringify({...locale.data, hereWeGo: 'test!'}, null, 4) : locale.data,
                    contentType: 'application/json',
                    contentEncoding: 'UTF-8'
                })
    
                const key = `locales/${locale.name}`
    
                locations[key] = buildGlobalPath(key, fileLocation)
    
                _resolve()
            } catch(error) {
                _reject(error)
            }
        }
    }
    
    const upload = params => {    
        return new Promise((_resolve, _reject) => {
            console.log(`${colors.yellow(`Uploading file: ${params.key}`)}`)
    
            const callback = (error, data) => {
                if (error) {
                    _reject(error)
                } else {
                    _resolve(data.Location)
                }
            }
    
            const uploadParams = {
                Body: params.data,
                Bucket: s3BucketName,
                Key: `${baseKey}/${params.key}`,
                ContentType: params.contentType,
                ContentEncoding: params.contentEncoding || 'UTF-8',
                ACL: 'public-read',
            }
    
            s3Bucket.upload(uploadParams, callback)
        })
    }
    
    const get = params => {
        return new Promise((resolve, reject) => {
            console.log(`${colors.yellow(`Getting file: ${params.key}`)}`)
    
            const callback = (error, data) => {
                if (error) {
                    reject(error)
                } else {
                    resolve({
                        key: params.key,
                        data: JSON.parse(data.Body)
                    })
                }
            }
    
            const getParams = {
                Key: params.key,
                Bucket: s3BucketName
            }
    
            s3Bucket.getObject(getParams, callback)
        })
    }
    
    const handleManifest = async () => {
        manifest.buildVersion = Date.now()
        manifest.graphic_url = locations['icon.png']
        manifest.thumbnail_url = locations['thumbnail.png']
        manifest.markdown_url = locations['markdown.md']
    
        const fileLocation = await upload({
            key: 'manifest.json',
            data: JSON.stringify(manifest, null, 4),
            contentType: 'application/json',
            contentEncoding: 'UTF-8'
        })
    
        // eslint-disable-next-line require-atomic-updates
        locations['manifest.json'] = buildGlobalPath('manifest.json', fileLocation)
    }
    
    const copyLocalesFromOlderVersion = versionToCopyFrom => {
        return new Promise((resolve, reject) => {
            const parsedCopyVersoin = parsePath(versionToCopyFrom)
    
            const callback = (error, paths) => {
                if (error) {                    
                    reject(error)
                } else {
                    if (Array.isArray(paths.Contents) && paths.Contents.length) {
                        let getPromises = []
                        
                        paths.Contents.forEach(content => {
                            if (!content.Key.endsWith('/default.json')) {
                                getPromises.push(new Promise((_resolve, _reject) => {
                                    const getParams = {
                                        key: content.Key
                                    }
        
                                    get(getParams)
                                        .then(_resolve)
                                        .catch(_reject)
                                }))
                            }
                        })
        
                        Promise.all(getPromises)
                            .then(results => {
                                console.log('\n')
    
                                const uploadPromises = results.map(item => {
                                    const localeToUpload = {
                                        name: item.key.replace(`${bundlePath}/${parsedCopyVersoin}/locales/`, ''),
                                        data: item.data
                                    }
    
                                    return new Promise(procesLocaleAndUpload(localeToUpload))
                                })
    
                                Promise.all(uploadPromises)
                                    .then(resolve)
                                    .catch(reject)
                            })
                            .catch(reject)
                    } else {
                        resolve()
                    }
                }
            }
        
            const listParams = {
                Delimiter: '/',
                Bucket: s3BucketName,
                Prefix: `${bundlePath}/${parsedCopyVersoin}/locales/`
            }
    
            s3Bucket.listObjects(listParams, callback)
        })
    }
    
    const start = async () => {
        console.log('\n')
        console.log(colors.bgWhite(colors.black(' -------------------- ')))
        console.log(colors.bgWhite(colors.black('  Plugin S3 uploader  ')))
        console.log(colors.bgWhite(colors.black(' -------------------- ')))
        console.log('\n')
    
        try {
            let filesToUpload = await getPluginFilenames(resolve(constants.DIST_PATH))
    
            console.log(colors.blue('----------------------------------------\n'))
            console.log(colors.blue(`${manifest.name} @${manifest.version}:\n\t- ${filesToUpload.join('\n\t- ')}`))
            console.log(colors.blue('\n----------------------------------------'))
            console.log('\n')
    
            filesToUpload = filesToUpload.filter(i => i !== 'manifest.json')
    
            console.log(colors.yellow('----------------------------------------\n'))
    
            const uploadPromises = filesToUpload.map(filename => new Promise(processFileAndUpload(filename)))
    
            console.log(colors.yellow('\n----------------------------------------'))
            console.log('\n')
            console.log(colors.yellow('----------------------------------------\n'))
    
            await Promise.all(uploadPromises)
    
            console.log(colors.yellow('\n----------------------------------------'))
    
            if (localesSourceVersion) {
                try {
                    console.log('\n')
                    console.log(colors.yellow('----------------------------------------\n'))
                    console.log(colors.yellow(`Copy available locales from version: ${localesSourceVersion} \n`))
    
                    await copyLocalesFromOlderVersion(localesSourceVersion)
                    console.log(colors.yellow('\n----------------------------------------'))
                    console.log('\n')
    
                } catch(error) {
                    console.log('\n')
                    console.log(colors.red('*******************'))
                    console.log(colors.red('       WARNING       '))
                    console.log(colors.red('*******************'))
                    console.log(colors.red(`Something went wrong while copying locales from version: ${localesSourceVersion} \n`))
                    console.log(error)
                    console.log(colors.red(`The upload won't be aborted, and it will continue...`))
                }
            }
    
            await handleManifest()
    
            console.log('\n')
            console.log(colors.green('----------------------------------------', '\n'))
            Object.entries(locations).forEach(([key, value]) => console.log(colors.green(`Successfully uploaded ${key}: \n\t- ${value} \n`)))
            console.log(colors.green('----------------------------------------'))
            console.log('\n')
        } catch(error) {
            console.log('\n')
            console.log(colors.red('*******************'))
            console.log(colors.red('       ERROR       '))
            console.log(colors.red('*******************'))
            console.log(colors.red('Something went wrong while uploading plugin files: \n'))
            console.log(error)
        }
    }
    
    start()
}