#!/usr/bin/env bash

# Read the environment variables
set -a
source .env
set +a

node ./scripts/release-branch.js $1 $2
