/*eslint-env node */
'use strict'

const readlineSync = require('readline-sync')
const semver = require('semver')
const execProcess = require('./exec-process')
const pjson = require('./../package.json')

const releases = ['--patch', '--minor', '--major']

const _getArgReleaseFlag = () => {
    for (let i = 0; i < process.argv.length; i++) {
        const arg = process.argv[i]

        if (releases.includes(arg)) {
            return arg.replace(/--/g, '')
        }
    }

    return null
}

const _isDryRun = () => {
    return process.argv.includes('--d')
}

const release = async() => {
    const version = pjson.version
    const release = _getArgReleaseFlag()

    if (!release) {
        console.error('Missing release flag argument. Valid values are --patch, --minor and --major')
        return process.exit(1)
    }

    if (_isDryRun()) {
        console.info('NOTE that this is a dry run. No release branch will be created!')
    }

    const newVersion = semver.inc(version, release)
    if (readlineSync.keyInYN(`Current version is ${version}. You are about to release version ${newVersion}. Do you wish to continue?`) === false) {
        // `N` key was pressed.
        console.info('Ok, bye!')
        return process.exit(1)
    }

    try {
        if (!_isDryRun()) {
            // Start release branch
            if (release === 'hotfix') {
                await execProcess.result(`git flow hotfix start ${newVersion}`)
            } else {
                await execProcess.result(`git flow release start ${newVersion}`)
            }

            console.info(`Started release branch for version ${newVersion}`)
        }
    } catch (e) {
        console.error(e)
        return process.exit(1)
    }

    console.info('Release branch handling done!')
}

// noinspection JSIgnoredPromiseFromCall
release()