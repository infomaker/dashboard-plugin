/**
 * Create an Application by exporting you function component or by extending the Application class
 * Read more about Application (https://docs.navigaglobal.com/dashboard-plugin/application)
*/

import { forwardRef } from 'react'
import { Plugin, GUI } from '@root'

import MyModal from '@components/MyModal'
import MyExampleComponent from '@components/MyExampleComponent'

import {
    StyledTitle
} from './style'

const ApplicationComponent = (props, ref) => {
    const { config } = props

    const handleOpenModal = () => {
        Plugin.openModal({
            title: 'My Modal',
            component: MyModal,
            props: {
                // pass props to MyModal component...
            }
        })
    }

    return (
        // Use @plugin_bundle_class and the bundle in the manifest will be used as your class
        <div
            ref={ref}
            className={'@plugin_bundle_class'}
        >
            <StyledTitle>
                {config.pluginTitle || 'hello world'}
            </StyledTitle>

            <GUI.Button
                size={'large'}
                onClick={handleOpenModal}
            >
                {'Open a modal'}
            </GUI.Button>

            <MyExampleComponent
                input={`My example component #1`}
            />
            
            <MyExampleComponent
                color={'blue'}
                input={`My example component #2`}
            />
        </div>
    )
}

const Application = forwardRef(ApplicationComponent)

export {
    Application
}