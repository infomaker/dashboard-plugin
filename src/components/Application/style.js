import styled from 'styled-components'

const StyledTitle = styled.p`
    color: gray;
    font-weight: 600;
`

export {
    StyledTitle
}