/**
 * Create an Agent by extending the Agent class
 * Read more about Agent (https://docs.navigaglobal.com/dashboard-plugin/agent)
 */

import { Plugin } from '@root'

class Agent extends Plugin.Agent {
    constructor() {
        super()

        this.connect()
    }

    /**
     * This is a example of a super simple agent. Your agent should do something more meaningful than this 😎
    */
    connect() {
        console.log('Beep beep, agent is connected...')
    }

    /**
     * close() method will automatically be called when the plugin is deactivated.
    */
    close() {
        console.log('Agent is closed')
    }
}

export {
    Agent
}