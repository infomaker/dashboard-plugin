/**
 * Create an Modal by extending the Modal class
 * Read more about Modal (https://docs.navigaglobal.com/dashboard-plugin/modal)
 */

import { Plugin } from '@root'

const GUI = Plugin.GUI

const treasures = ["🐢", "🦂", "👑", "🐌", "💍"]

const Modal = () => {

    return (
        <GUI.Section title="You found a modal">
            <GUI.Paragraph
                text={`
                    You opened it and got 
                    ${(Math.floor(Math.random() * 5) + 1)} 
                    gems and a 
                    ${treasures[Math.floor(Math.random() * treasures.length)]}
                `}
            />
        </GUI.Section>
    )
}

export default Modal