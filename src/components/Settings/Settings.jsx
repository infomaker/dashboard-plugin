/**
 * Create settings for you plugin by extending the Dashboard.Settings component
*/

import { Plugin } from '@root'

const GUI = Plugin.GUI

class Settings extends Plugin.Settings {
    /**
     * Plugin settings will be displayed in the store. These settings will be available for Agent, Widget and Application.
    */
    plugin() {
        return (
            <GUI.Wrapper>
                <GUI.ConfigInput
                    name={'Title'}
                    ref={ref => this.handleRefs(ref, 'pluginTitle')}
                />
            </GUI.Wrapper>
        )
    }

    /*
     * Application settings will be displayed in application settings mode. These settings will only be available for the application.
    */
    application() {
        return (
            <GUI.Wrapper>
                <GUI.ConfigInput
                    name={'Title'}
                    ref={ref => this.handleRefs(ref, 'pluginTitle')}
                />
            </GUI.Wrapper>
        )
    }
}

export {
    Settings
}