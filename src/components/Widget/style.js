import styled from 'styled-components'

const Wrapper = styled.div`
    display: flex;
    flex-direction: column;

    min-width: 165px;
    min-height: 55px;
`

export {
    Wrapper
}