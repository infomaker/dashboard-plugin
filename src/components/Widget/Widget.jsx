/**
 * Create a Widget by exporting a fnction component or by extending the Widget class
 * Read more about Widget (https://docs.navigaglobal.com/dashboard-plugin/widget)
*/

import { GUI } from '@root'
import { forwardRef } from 'react'

import {
    Wrapper
} from './style'

const WidgetComponent = (props, ref) => {
    return (
        <Wrapper ref={ref}>
            <GUI.ListItem
                asMenuItem
                label={'Hello world!'}
                onClick={() => window.open('https://www.google.se/?gws_rd=ssl#q=hello+world')}
            />
        </Wrapper>
    )
}

const Widget = forwardRef(WidgetComponent)

export {
    Widget
}