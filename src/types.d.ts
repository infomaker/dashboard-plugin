import * as GUIElements from '@infomaker/types-style-guide'

declare module "Dashboard"

declare module "Dashboard/modules" {
    const GUI: typeof GUIElements
    const Utility: any
}

declare global {
    interface Window {
        dashboard_config: any
    }
}

type User = {
    groups: Array<string>,
    info: {
        given_name: string,
        family_name: string,
        email: string
    },
    isServiceAdmin: boolean,
    name: string,
    organisation: string,
    permissions: Array<string>,
    sub: string,
    unit: string,
    unitId: string,
    units: Array<string>
}

declare module "Dashboard/plugin" {
    const DashboardPlugin = class {
        constructor(bundle: string)

        Component
        GUI
        ViewUtil
        Modal
        Agent
        Health
        Widget
        Settings
        Portal
        Application
        Popout
        PopoutComponent

        Sheet

        moment
        MODALSIZE
        createUUID: () => string
        getTextDirection: () => string
        CopyToClipboard
        getKeyCharFromCode: (code: number) => string
        getKeyCodeFromChar: (char: string) => number
        event

        getLanguage: () => string
        getTimeFormat: () => string

        Logger

        buildRouteUrlWithDispatchableEvent

        cache: {
            load: () => any,
            save: () => boolean, 
            persist: () => boolean, 
            remove: () => void
        }

        store: {
            load: () => any,
            save: () => boolean, 
            persist: () => boolean, 
            remove: () => void
        }

        encrypt: Promise<any>
        decrypt: Promise<any>

        request: any

        setHealth: (isHealthy: boolean) => void

        modal: {
            open: ({ component, props, onModalRendered }: {
                component: any
                props: any
                onModalRendered: any
            }) => void
            close: () => void
            openApplication: ({ bundle, onApplicationRendered, title, modalSize, modalClass, modalMenu, callback, props }: {
                bundle: any
                onApplicationRendered: any
                title: any
                modalSize: any
                modalClass: any
                modalMenu: any
                callback: any
                props: any
            }) => void
        }
        openModal: (modelParams: any) => void
        closeModal: () => void

        confirm: {
            add: (confirmPayload: object) => void
            remove: () => void
        }

        applicationConfirm: {
            add: (id: string, confirmPayload: object) => void
            remove: () => void
        }

        notifications: {
            add: (payload: any) => void
            remove: (payload: any) => void
        }

        withUser: (receiver: any) => any

        getUser: () => User
        getAction: (id: any) => any
        getPortal: (id: any) => React.ForwardRefExoticComponent<Pick<any, string | number | symbol> & React.RefAttributes<any>>
        getConfig: (selector?: any) => any
        getLocalize: (selector?: any) => any
        getAvailableActions: (identifier?: any) => any
        getAvailablePortals: (identifier?: any) => any

        useModal: () => any
        useSheet: () => any
        useUser: () => any
        useApplication: (selector?: any) => any
        useLocalization: (selector?: any) => any
        useConfig: (selector?: any) => any
        useLocalize: (selector?: any) => any
        useMappings: () => object
        useApplicationConfig: (applicationId: any) => any

        register: (configurations: object) => void
        hasPermission: (id: any) => boolean

    }

    export = DashboardPlugin
}