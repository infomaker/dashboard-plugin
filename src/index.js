/*
|--------------------------------------------------------------------------
| index.js
|--------------------------------------------------------------------------
|
| Welcome to your plugin.
| Documentation can be found at (https://docs.navigaglobal.com/dashboard-plugin)
|
*/

import {
    GUI,
    Utility
} from 'Dashboard/modules'

import DashboardPlugin from 'Dashboard/plugin'

/*
| import '@root/style.css'
|
| Uncomment this import to load style.css IF you are using css in your plugin
| And set "withCSS" to true in manifest.json file
| We recommend you to use styled-components, more info: https://styled-components.com
*/

const Plugin = new DashboardPlugin('@plugin_bundle')

const registerPlugin = () => {
    const { Agent } = require('@components/Agent')
    const { Widget } = require('@components/Widget')
    const { Settings } = require('@components/Settings')
    const { Application } = require('@components/Application')

    /**
     * Register your plugin in Dashboard.
     * With register function you can register more than a "Component" to render in Dashobard,
     * you can register requirements, defaultConfig and more check out what you can register (https://docs.navigaglobal.com/dashboard-plugin/register)
    */
    Plugin.register({
        // Only one of these are actually required. If you are developing a widget, just remove the application and agent.
        agent: Agent,
        widget: Widget,
        application: Application,

        // Settings is optional.
        settings: Settings
    })
}

registerPlugin()

if (module.hot && typeof module.hot.accept === 'function') {
    module.hot.accept()
}

export {
    GUI,
    Plugin,
    Utility
}