/* globals process, __dirname */

/*
|--------------------------------------------------------------------------
| Server.js
|--------------------------------------------------------------------------
|
| This is your local server. Kickstart it by running npm server and it will serve your plugin so that you can install it
| on dev.dashboard.infomaker.io.
|
*/

const express = require('express')
const app = express()
const cors = require('cors')
const manifest = require('./manifest.json')
const ip = require('ip')

process.env.PORT = process.env.PORT || 7000

const PORT = process.env.PORT

const useHOT = process.env.HOT === "1"
const useSSL = process.env.SSL === "1"
const useDist = process.env.DIST === "1"

const protocol = useSSL ? 'https://' : 'http://'
const ipAddress = ip.address()

const path = `${protocol}${ipAddress}:${PORT}`

app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*")
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, X-Access-Token")
    res.header("Access-Control-Allow-Methods", "DELETE, GET, HEAD, POST, PUT, OPTIONS, TRACE")

    if (req.url.startsWith('/manifest.json')) {
        const updatedManifest = {
            ...manifest,
            graphic_url: `${path}/icon.png`,
            markdown_url: `${path}/markdown.md`,
            thumbnail_url: `${path}/thumbnail.png`
        }

        res.contentType('application/json')
            .status(200)
            .send(updatedManifest)
    } else {
        next()
    }
})

app.use(cors())

if (useHOT) {
    const Webpack = require('webpack')
    const WebpackDevMiddleware = require("webpack-dev-middleware")
    const WebpackHotMiddleware = require("webpack-hot-middleware")
    const webpackConfig = require("./__tooling__/webpack/webpack.dev.hot.config.js")

    const Compiler = Webpack(webpackConfig)

    app.use(WebpackDevMiddleware(Compiler, {
        hot: true,
        logTime: true,
        historyApiFallback: true,
        stats: webpackConfig.stats,
        publicPath: webpackConfig.output.publicPath
    }))

    app.use(WebpackHotMiddleware(Compiler, {
        path: "/__webpack_hmr",
    }))
} else if (useDist) {
    app.use(express.static(__dirname + '/dist'))
} else {
    app.use(express.static(__dirname + '/build'))
}

if (useSSL) {
    const fs = require('fs')
    const https = require('https')

    https.createServer({
        key: fs.readFileSync('server.key'),
        cert: fs.readFileSync('server.cert')
    }, app).listen(PORT, () => {
        console.log(`\n🎉  ${manifest.name} manifest.json served at ${path}/manifest.json`)
    })
} else {
    const http = require('http').Server(app)

    http.listen(PORT, () => {
        console.log(`\n🎉  ${manifest.name} manifest.json served at ${path}/manifest.json`)
    })
}