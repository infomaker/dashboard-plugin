module.exports = function (api) {
    let presets = [
        '@babel/preset-env',
        '@babel/preset-react'
    ]

    let plugins = []
    
    if (api.env('development')) {
        // add development specific presets/plugins here
        plugins.push(["babel-plugin-styled-components", {
            ssr: false
        }])

    } else if (api.env('production')) {
        // add production specific presets/plugins here
    }

    return {
        presets,
        plugins
    }
}