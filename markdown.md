# Dashboard Plugin
Dashboard Plugin is a starter project for developing Dashboard plugins that contains all the tools you need for writing awesome plugins.

## Wiki
[Learn all the things](https://docs.navigaglobal.com/dashboard-plugin)

## License
Dashboard Plugin is released under the [MIT](http://www.opensource.org/licenses/MIT) License.