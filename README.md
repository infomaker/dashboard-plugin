# Dashboard Plugin
Dashboard Plugin is a starter project for developing Dashboard plugins that contains all the tools you need for writing awesome plugins.

## Wiki
[Learn all the things](https://docs.navigaglobal.com/dashboard-plugin)

### Clone the project
```bash
$ git clone git@bitbucket.org:infomaker/dashboard-plugin.git
$ cd dashboard-plugin
```

### Setup the project
1. Remove the `.git` directory
2. Reset version in `package.json` to 0.0.0
3. Rename the project directory
4. Install git-flow `npm install git-flow`
5. Run `git flow init` command in the project
6. Install dependencies `npm install`
7. Enable Bitbucket pipelines and add the environment variables (found under "Settings > Deployments") 
specified in table below (this step for deploying the plugin to S3 buckets)
7. Start developing!

## Run
```bash
$ npm install
$ npm run start
```

## Test
```bash
$ npm install
$ npm run test
```

## Styling
We've chosen to work with styled components (https://www.styled-components.com/docs) and therefore we install the dependencies for you. But nothing stops you from using any other type of style compling.


### Bitbucket environment variables
#### Required
| Name | Description |
| :--- | :---------- |
| S3_BUCKET_NAME        | Internal target bucket for built plugin files |
| S3_ACCESS_KEY         | Access Key for internal target S3 |
| S3_SECRET_ACCESS_KEY  | Secret Access Key for internal target S3 |
| S3_REGION             | AWS Region where target bucket is located |

#### Optional
| GLOBAL_CLOUDFRONT_PATH  | The global CloudFront path to be used for urls in manifest.json |

#### Inputed
| Name | Description |
| :--- | :---------- |
| VERSION_OVERRIDE        | Set custom version to override version upload path |
| LOCALES_SOURCE_VERSION  | The source version to copy all the locales from |


#### Manifest
The build and deploy workflow depends on a manifest file (`manifest.json`) being present (i.e. mandatory) in the project in root dir.
[Please see manifest structure](https://docs.navigaglobal.com/dashboard-plugin/manifest-structure)

The version that you create must correlate to the release that you are planning to do, i.e. if you are planning to 
release a hotfix you should create a version which bumps the "PATCH" part of the version, if you are planning a minor 
version the "MINOR" part of the version should be bumped and if you are doing a major version the "MAJOR" part needs 
to be incremented.

## Releases
### Using Bitbucket-Pipelines
* Enable Pipelines in Bitbucket under your plugin repo page > Settings > Pipelines > Enable Pipelines.
* Rename `bitbucket-pipelines-template.yml` in the root dir to `bitbucket-pipelines.yml` and configure it the way you choose (if needed).
* Make sure that you follow git-flow, i.e. if you are about to do a "hotfix" you should be on master branch, if making a release you should be on develop.

With that out of the way, follow these steps:

1.  Depending on the type of release you are about to perform, execute `npm run-script release:hotfix`, `npm run-script release:minor` or `npm run-script release:major`, 
The script will (if everything looks fine) create a release branch in git, run test, build and validate the plugin artifacts, 
update the manifest. The scripts will also commit the updated files
2.  Execute the "finish release" command in git-flow and push the tag (i.e. version) to `develop` and `master`
3.  The bitbucket pipelines will upload your plugin to a S3 bucket.

#### NOTE:
Our `bitbucket-pipelines-template.yml` is configured in a way that it will deploy and run a certain steps on every git push you do to the master brunch which they are:

- install all the dependencies
- run all the tests
- build your code
- validate files
- upload the files to S3 bucket

**For more info go to Plugin deployments in Dashboard-Plugin documentation on GitBook: [Here](https://docs.navigaglobal.com/dashboard-plugin/plugins-deployments)**

you can see the scripts that `bitbucket-pipelines-template.yml` will run in scripts folder in the project root

### Using builtIn upload.js script
[Please see s3Upload doc](https://docs.navigaglobal.com/dashboard-plugin/s3-upload)

## License
Dashboard Plugin is released under the [MIT](http://www.opensource.org/licenses/MIT) License.