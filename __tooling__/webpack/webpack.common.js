const path = require('path')
const postcssPresetEnv = require('postcss-preset-env')
const ESLintPlugin = require('eslint-webpack-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const MiniCssExtractPlugin = require("mini-css-extract-plugin")

const rootDir = path.resolve(__dirname, "..", "..")
const manifest = require(path.resolve(rootDir, 'manifest.json'))

const styleConfig = manifest.withCSS !== false ? {
    loaders: [
        {
            enforce: 'pre',
            test: /\.(css)$/,
            loader: 'string-replace-loader',
            exclude: /node_modules/,
            options: {
                multiple: [
                    {
                        flags: 'g',
                        search: '@plugin_bundle_class',
                        replace: manifest.bundle.replace(/\./g, '-')
                    }
                ]
            }
        },
        {
            test: /\.css$/,
            use: [
                'style-loader',
                MiniCssExtractPlugin.loader,
                {
                    loader: "css-loader",
                    options: {
                        modules: false,
                        importLoaders: 1,
                        sourceMap: false
                    }
                },
                {
                    loader: "postcss-loader",
                    options: {
                        plugins: () => [
                            postcssPresetEnv()
                        ]
                    }
                }
            ]
        }
    ],
    plugins: [
        new MiniCssExtractPlugin({
            filename: 'style.css'
        })
    ]
} : {
    loaders: [],
    plugins: []
}

module.exports = {
    entry: path.resolve(rootDir, "src/index.js"),

    resolve: {
        extensions: ['.js', '.json', '.jsx', '.ts', '.tsx'],
        alias: {
            '@root': path.resolve(rootDir, "src/"),
            '@utils': path.resolve(rootDir, "src/utils/"),
            '@services': path.resolve(rootDir, "src/services/"),
            '@components': path.resolve(rootDir, "src/components/")
        }
    },

    externals: {
        "react": "React",
        "react-dom": "ReactDOM",
        "Dashboard/plugin": "DashboardPlugin",
        "Dashboard/modules": "DashboardModules",
        "styled-components": "StyledComponents"
    },

    module: {
        rules: [
            {
                test: /\.(ts|tsx)$/,
                exclude: /node_modules/,
                use: [
                    {
                        loader: 'ts-loader',
                        options: {
                            configFile: path.resolve(rootDir, 'tsconfig.json')
                        }
                    }
                ]
            },
            {
                test: /\.(png|woff|woff2|eot|ttf|svg)$/,
                use: [
                    {
                        loader: 'url-loader',
                        options: {
                            limit: 100000
                        }
                    }
                ]
            },
            {
                enforce: 'pre',
                test: /\.(js|jsx)$/,
                loader: 'string-replace-loader',
                exclude: /node_modules/,
                options: {
                    multiple: [
                        {
                            flags: 'g',
                            search: '@plugin_bundle_version',
                            replace: manifest.version.replace(/\./g, '-')
                        },
                        {
                            flags: 'g',
                            search: '@plugin_bundle',
                            replace: manifest.bundle,
                        }
                    ]
                }
            },
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        configFile: path.resolve(rootDir, 'babel.config.js')
                    }
                }
            },
            ...styleConfig.loaders
        ]
    },

    stats: {
        colors: true,
        errors: true,
        warnings: true,
        errorDetails: true
    },

    plugins: [
        ...styleConfig.plugins,

        new ESLintPlugin({
            extensions: ['js', 'jsx', 'ts', 'tsx'],
            failOnWarning: false,
            failOnError: true
        }),

        new CopyWebpackPlugin({
            patterns: [
                {
                    from: 'manifest.json',
                    to: '.'
                },
                {
                    noErrorOnMissing: true,
                    from: 'markdown.md',
                    to: '.'
                },
                {
                    noErrorOnMissing: true,
                    from: 'icon.png',
                    to: '.'
                },
                {
                    noErrorOnMissing: true,
                    from: 'thumbnail.png',
                    to: '.'
                },
                {
                    noErrorOnMissing: true,
                    from: 'src/locales',
                    to: './locales'
                }
            ]
        })
    ]
}