/* globals __dirname */

const path = require('path')
const { merge } = require('webpack-merge')

const common = require('./webpack.common.js')

const cfg = merge(common,
    {
        mode: 'development',
        output: {
            filename: 'index.js',
            path: path.resolve(__dirname, '..', '..', 'build'),
            publicPath: '/'
        },
        devtool: 'eval-source-map',
        plugins: []
    }
)

module.exports = cfg